package org.nrg.xnat.turbine.modules.actions;

import java.io.File;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatImageassessordata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.utils.WorkflowUtils;

public class UploadKeyImages extends SecureAction {

    /* (non-Javadoc)
     * @see org.apache.turbine.modules.actions.VelocitySecureAction#doPerform(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    @Override
    public void doPerform(RunData data, Context context) throws Exception {
        ParameterParser params = data.getParameters();
        
        XFTItem radI = (XFTItem)TurbineUtils.GetItemBySearch(data,true);
        PersistentWorkflowI wrk= PersistentWorkflowUtils.buildOpenWorkflow(TurbineUtils.getUser(data), radI, EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_FORM, "Key Image Upload"));
        
        try {
            //byte[] bytes = params.getUploadData();
            //grab the FileItems available in ParameterParser
            FileItem fi = params.getFileItem("image_archive");
            if (fi != null)
            {
                String filename = fi.getName();                
                
                XnatImageassessordata rad = new XnatImageassessordata(radI);
                XnatImagesessiondata img = rad.getImageSessionData();
                
                              
                String archivePath = img.getArchivePath();
                
                if (!archivePath.endsWith("/")){
                    archivePath+="/";
                }
                
                archivePath +=img.getArchiveDirectoryName() + "/ASSESSORS/RAD/";
                archivePath +=rad.getArchiveDirectoryName() + "/";
                
                File dest = new File(archivePath);
                dest.mkdirs();
                
                
                                
                //BUILD TEMPORARY PATH

                UserI user = TurbineUtils.getUser(data);
                
                System.out.println("Uploading file.");
                //upload file
                
                
                int index = filename.lastIndexOf("\\");
                if (index< filename.lastIndexOf("/"))index = filename.lastIndexOf("/");
                if(index>0)filename = filename.substring(index+1);
                
                File uploaded = new File(archivePath + filename) ;
                fi.write(uploaded);

                String label = data.getParameters().getString("label");
                
                XnatResource resource = new XnatResource((UserI)user);
                resource.setFormat("GIF");
                                
                resource.setUri(uploaded.getAbsolutePath());
                resource.setLabel(label);
                rad.setOut_file(resource);
                
                rad.save(user, true, true,wrk.buildEvent());
                
                System.out.println("File Upload Complete.");
                           
                data.setMessage("File Uploaded.");
                TurbineUtils.SetSearchProperties(data,radI);
                data.setScreenTemplate("UploadKeyImages.vm");
                
                WorkflowUtils.complete(wrk, wrk.buildEvent());
            
                fi.delete();
            }
        } catch (Exception e) {
            WorkflowUtils.fail(wrk, wrk.buildEvent());
            error(e,data);
        }
    }

}
